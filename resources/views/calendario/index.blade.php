@extends('layouts.app')

@section('content')
<div class="container">
    <div id='calendar'></div>
</div>
@endsection

@section('scripts')
<script>
$('#calendar').fullCalendar({
  weekends: false,
  events: [
    @foreach($menus as $menu)
    {
      title: "{{ $menu->titulo }}",
      start: "{{ $menu->dia }}"
    },
    @endforeach
  ]
});
</script>
@endsection