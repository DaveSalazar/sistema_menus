@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 align="center">Registro de Menus</h1>
        </div>
    </div>
    <a href="{{ url('/menus/create') }}" class="btn btn-primary" style="top: 35%">Nuevo registro</a>
    <hr>
    
    <br>
    <div class="row">
        <table id="tblComidas">
            <thead>
            <tr>
                <td>ID</td>
                <td>Titulo</td>
                <td>Sopa</td>
                <td>Segundo</td>
                <td>Dia</td>
                <td>opciones</td>
            </tr>
            </thead>
            <tbody>
                @foreach($menus as $menu)
                <tr>
                    <td>{{ $menu->id }}</td>
                    <td>{{ $menu->titulo }}</td>
                    <td>{{ $comidas->where('id', $menu->id_sopa)->first()->nombre }}</td>
                    <td>{{ $comidas->where('id', $menu->id_segundo)->first()->nombre }}</td>                   
                    <td>{{ $menu->dia }}</td>
                    <td>
                        <a href="#" class="btn btn-success">Editar</a>
                        <a href="#" class="btn btn-danger">Eliminar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('#tblComidas').DataTable();
</script>
@endsection