@extends('layouts.app')

@section('content')

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Nueva Comida</div>
        <div class="panel-body">
            <form action="{{ url('/menus') }}" method="POST">
            {{ csrf_field() }}

                <div class="row">
                    <div class="col-lg-12">
                        <label for="">Titulo</label>
                        <input type="text" name="titulo"  class="form-control" required>
                        <label for="">Sopa: </label>
                        <select name="sopa" id="" class="form-control">
                            @foreach($comidas->where('tipo', 'Sopa') as $comida)
                            <option value="{{ $comida->id }}">{{ $comida->nombre }}</option>
                            @endforeach
                        </select>
                        <label for="">Segundo: </label>
                        <select name="segundo" id="" class="form-control">
                            @foreach($comidas->where('tipo', 'Segundo') as $comida)
                            <option value="{{ $comida->id }}">{{ $comida->nombre }}</option>
                            @endforeach
                        </select>
                        <label for="">Dia:</label>
                        <input type="date" name="dia" class="form-control" required>
                    </div>        
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-offset4 col-lg-3">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#imagen").fileinput({
        allowedFileExtensions: ["jpeg", "jpg", "png"],
    });        
</script>
@endsection