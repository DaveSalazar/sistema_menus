@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 align="center">Registro de Comidas</h1>
        </div>
    </div>
    <a href="{{ url('/comidas/create') }}" class="btn btn-primary" style="top: 35%">Nuevo registro</a>
    <hr>
    
    <br>
    <div class="row">
        <table id="tblComidas">
            <thead>
            <tr>
                <td>ID</td>
                <td>Nombre</td>
                <td>Descripcion</td>
                <td>Tipo</td>
                <td>Imagen</td>
                <td>opciones</td>
            </tr>
            </thead>
            <tbody>
                @foreach($comidas as $comida)
                <tr>
                    <td>{{ $comida->id }}</td>
                    <td>{{ $comida->nombre }}</td>
                    <td>{{ $comida->descripcion }}</td>
                    <td>{{ $comida->tipo }}</td>
                    <td>{{ $comida->imagen }}</td>
                    <td>
                        <a href="#" class="btn btn-success">Editar</a>
                        <a href="#" class="btn btn-danger">Eliminar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('#tblComidas').DataTable();
</script>
@endsection