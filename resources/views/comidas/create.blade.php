@extends('layouts.app')

@section('content')

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Nueva Comida</div>
        <div class="panel-body">
            <form action="{{ url('/comidas') }}" method="POST">
            {{ csrf_field() }}

                <div class="row">
                    <div class="col-lg-6">
                        <label for="">Nombre: </label>
                        <input type="text" name="nombre" class="form-control" required>
                        <label for="">Descripcion: </label>
                        <input type="text" name="descripcion" class="form-control" required>
                        <label for="">Tipo: </label>
                        <select name="tipo" id="" class="form-control">
                            <option value="Sopa">Sopa</option>
                            <option value="Segundo">Segundo</option>
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <input id="imagen" type="file" class="file" data-preview-file-type="image">
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-lg-offset4 col-lg-3">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#imagen").fileinput({
        allowedFileExtensions: ["jpeg", "jpg", "png"],
    });        
</script>
@endsection