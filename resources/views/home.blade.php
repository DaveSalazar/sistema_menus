@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">SERVICIO DE ALMUERZOS</div>

                <div class="panel-body">
                   <div class="row">
                    <div class="col-lg-6">
                        <h3>Misión</h3>
                        <ul>
                            <li> Crecer sostenida y ordenadamente.​</li>
                            <li> Administrar con eficiencia y profesionalismo.</li>
                            <li> Generar valor para nosotros y la comunidad.</li>
                            <li> Seguir fielmente los valores corporativos.</li>
                        </ul>
                        <h3>Visión</h3>
                        <ul>
                            <li> Ser la mejor tienda departamental para las personas, familias y comunidad que quieren enriquecer su vida.</li>
                        </ul>
                        <h3>Valores</h3>
                        <ul>
                            <li> Ante todo actuamos con integridad.</li>
                            <li>  Estamos orientados a satisfacer al cliente.</li>
                            <li> Nos comunicamos oportuna y sinceramente.</li>
                            <li> Creemos en nuestra empresa y su gente.</li>
                            <li> Trabajamos en equipo.</li>
                            <li> Estamos comprometidos con los resultados.</li>
                        </ul>
                        
                    </div>
                    <div class="col-lg-6">
                        <img src="{{ asset('img/dprati.jpg') }}" style="max-width:100%">
                    </div>
                    
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
