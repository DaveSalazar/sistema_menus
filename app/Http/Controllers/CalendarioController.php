<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Auth;
class CalendarioController extends Controller
{
    public function index(){
        $menus = Menu::where('id_user', Auth::user()->id)->get();
        return view('calendario.index', compact('menus'));
    }
}
