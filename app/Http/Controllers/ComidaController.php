<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comida;
class ComidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comidas = Comida::all();

        return view('comidas.index', compact('comidas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comidas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //dd($request->all());

            $comida = new Comida;

            $comida->nombre = $request->nombre;
            $comida->descripcion = $request->descripcion;
            $comida->tipo = $request->tipo;
           
    
            if ($request->hasFile('imagen') && $request->file('imagen')->isValid() ) {
                $nombre_adjunto = $request->file('imagen')->getClientOriginalName();
                $request->file('imagen')->storeAs('public/comidas', $request->file('imagen')->getClientOriginalName());
                $comida->imagen = $nombre_adjunto;
            }
    
            $comida->save();
    
            return redirect('/comidas');
        }catch(Exception $e){
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
