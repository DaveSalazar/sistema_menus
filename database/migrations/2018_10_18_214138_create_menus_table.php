<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sopa')->unsigned();
            $table->integer('id_segundo')->unsigned();
            $table->date('dia');
            $table->timestamps();

            $table->foreign('id_sopa')->references('id')->on('comidas')->onDelete('cascade');
            $table->foreign('id_segundo')->references('id')->on('comidas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
